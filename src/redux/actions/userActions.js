import { userService } from "../../service/userService";
import { SET_USER_INFOR } from "../constants/userConstants";

export const setUserInforAction = (user) => {
  return {
    type: SET_USER_INFOR,
    payload: user,
  };
};

let handleLoginSuccess = (value) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_INFOR,
      payload: value,
    });
  };
};
export const setUserInforActionService = (
  dataLogin,
  handleSuccess,
  handleFail
) => {
  return (dispatch) => {
    userService
      .dangNhap(dataLogin)
      .then((res) => {
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        // handleLoginSuccess(res.data.content);
        handleSuccess();
        console.log(res);
      })
      .catch((err) => {
        console.log(err);

        handleFail("Đã có lỗi xảy ra");
      });
  };
};
