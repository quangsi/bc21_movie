import React from "react";
import FormDangKi from "./FormDangKi/FormDangKi";

export default function DangKiPage() {
  return (
    <div className=" bg-orange-300 flex items-center w-full h-screen">
      <div className="flex">
        <div className="w-1/2 h-full"></div>
        <div className="w-1/2 h-full">
          <FormDangKi />
        </div>
      </div>
    </div>
  );
}
