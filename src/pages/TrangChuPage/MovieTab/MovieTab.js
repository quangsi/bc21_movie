import { Tabs } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { movieService } from "../../../service/movieService";

let { TabPane } = Tabs;
export function MovieTab() {
  const [dataRaw, setDataRaw] = useState([]);
  useEffect(() => {
    movieService
      .getMoviesByTheaters()
      .then((res) => {
        console.log(res);
        setDataRaw(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const onChange = (key) => {
    console.log(key);
  };

  let renderContent = () => {
    return dataRaw.map((heThonRap, index) => {
      return (
        <TabPane
          key={index}
          tab={<img className="w-10 h-10" src={heThonRap.logo} />}
        >
          <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
            {heThonRap.lstCumRap.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div className="w-48 whitespace-normal text-left ">
                      <p>{cumRap.tenCumRap}</p>
                      <p className=" ">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="space-y-5"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return (
                        <div className="border-gray-400 border-b-4 p-3">
                          <p>{phim.tenPhim}</p>
                          {/* "lstLichChieuTheoPhim": [ */}
                          <div className="grid grid-cols-3 gap-5">
                            {phim.lstLichChieuTheoPhim.map((item, index) => {
                              if (index < 6) {
                                return (
                                  <span className="px-5 py-2 bg-red-500 text-white rounded text-center font-medium ">
                                    {moment(item.ngayChieuGioChieu).format(
                                      "DD/MM/YYYY - HH:mm"
                                    )}
                                  </span>
                                );
                              }
                            })}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <div>
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
      >
        {renderContent()}
        {/* <TabPane tab="Tab 1" key="1">
          Content of Tab Pane 1
        </TabPane>
        <TabPane tab="Tab 2" key="2">
          Content of Tab Pane 2
        </TabPane>
        <TabPane tab="Tab 3" key="3">
          Content of Tab Pane 3
        </TabPane> */}
      </Tabs>
    </div>
  );
}
