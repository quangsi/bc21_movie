import { Progress } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../service/movieService";

export default function ChiTietPhimPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchMovie = async () => {
      let result = await movieService.getDetailMovie(id);
      console.log("result: ", result);
      setMovie(result.data.content);
    };
    fetchMovie();
  }, []);
  //   {
  //     "maPhim": 8798,
  //     "tenPhim": "Cậu Ấy Không Phải Tôi - Not Me (2021))",
  //     "biDanh": "cau-ay-khong-phai-toi-not-me-2021-",
  //     "trailer": "https://www.youtube.com/watch?v=Q91hKXjq_3s",
  //     "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/cau-ay-khong-phai-toi-not-me-2021-_gp01.jpg",
  //     "moTa": "Cậu Ấy Không Phải Tôi - Not Me (2021) là bộ phim truyền hình Thái Lan, do đạo diễn Nuchie Anucha Boonyawatana cầm trịch sản xuất. Phim xoay quanh cặp đôi Đen và Trắng là cặp song sinh có mối liên hệ mạnh mẽ và hành trình tìm ra sự thật để báo thù về cái chết của người anh em còn lại của mình.",
  //     "maNhom": "GP01",
  //     "hot": true,
  //     "dangChieu": true,
  //     "sapChieu": false,
  //     "ngayKhoiChieu": "2022-05-23T20:05:12.163",
  //     "danhGia": 8
  // }
  return (
    <div className="p-10 ">
      <img src={movie.hinhAnh} className="w-40" alt="" />
      <p>{movie.tenPhim}</p>
      <p>{movie.moTa}</p>

      <Progress
        type="circle"
        percent={movie.danhGia * 10}
        format={(number) => {
          return <span className="text-green-500"> {number / 10} điểm</span>;
        }}
      />
    </div>
  );
}
